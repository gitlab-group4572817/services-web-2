#!/bin/bash

echo "Vérification de l'espace disque avant nettoyage"
df -h

echo "Suppression des fichiers temporaires"
sudo rm -rf /tmp/*
sudo rm -rf /var/tmp/*

echo "Suppression des anciens journaux"
sudo find /var/log -type f -name "*.log" -delete

echo "Nettoyage des anciens conteneurs et images Docker"
docker system prune -af
docker volume prune -f

echo "Suppression des paquets inutilisés"
sudo apt-get autoremove -y
sudo apt-get clean

echo "Suppression des versions anciennes des Snap packages"
sudo snap list --all | awk '/désactivé|désactivé/ {print $1, $3}' |
    while read snapname revision; do
        sudo snap remove "$snapname" --revision="$revision"
    done

echo "Suppression des fichiers de cache GitLab Runner"
sudo rm -rf /home/ubuntu/builds/*

echo "Vérification de l'espace disque après nettoyage"
df -h
