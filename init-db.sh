#!/bin/bash

# Extraire le hostname et le port
DB_HOST=$(echo $WORDPRESS_DB_HOST | cut -d':' -f1)
DB_PORT=$(echo $WORDPRESS_DB_HOST | cut -d':' -f2)

DB_USER=${WORDPRESS_DB_USER}
DB_PASSWORD=${WORDPRESS_DB_PASSWORD}
DB_NAME=${WORDPRESS_DB_NAME}

# Vérifier que toutes les variables sont définies
echo "WORDPRESS_DB_HOST: $WORDPRESS_DB_HOST"
echo "DB_HOST: $DB_HOST"
echo "DB_PORT: $DB_PORT"
echo "WORDPRESS_DB_USER: $WORDPRESS_DB_USER"
echo "WORDPRESS_DB_PASSWORD: $WORDPRESS_DB_PASSWORD"
echo "WORDPRESS_DB_NAME: $WORDPRESS_DB_NAME"

# Vérifier si la base de données existe déjà
DB_EXISTS=$(mysql -h $DB_HOST -P $DB_PORT -u $DB_USER -p$DB_PASSWORD -e "SHOW DATABASES LIKE '$DB_NAME';" | grep "$DB_NAME")

if [ -n "$DB_EXISTS" ]; then
  echo "Database $DB_NAME already exists. Skipping creation."
else
  echo "Database $DB_NAME does not exist. Creating..."
  mysql -h $DB_HOST -P $DB_PORT -u $DB_USER -p$DB_PASSWORD -e "CREATE DATABASE $DB_NAME;"
  echo "Database $DB_NAME created."
fi

echo "Database check completed"
