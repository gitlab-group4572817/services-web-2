Pipeline de CI/CD GitLab
========================

Ce fichier `gitlab-ci.yml` décrit un pipeline complet pour déployer et configurer une application WordPress avec des outils de monitoring comme Prometheus et Grafana. Les variables sont définies dans gitlab et générées dans les artifacts.

Variables
---------

Les variables globales utilisées dans le pipeline incluent l'espace de noms Kubernetes, le pilote Docker, et les identifiants d'administration de Grafana.

Stages
------

Les différentes étapes (stages) du pipeline sont :

*   **build** : Construction des images Docker nécessaires.
*   **app** : Déploiement et configuration de l'application WordPress.
*   **monitoring** : Déploiement et configuration des outils de monitoring.
*   **backup** : Sauvegardes de la base de données et des volumes persistants (commentée dans le script).

Étapes du Pipeline
------------------

### Build

**Construire l'image Docker de WordPress**

Cette étape consiste à construire l'image Docker pour WordPress en utilisant Docker in Docker (dind). L'image construite est ensuite poussée vers le registre Gitlab.

### App

**Déploiement de WordPress**

Déployer WordPress en utilisant Helm et l'image WordPress du registre Gitlab. Cette étape installe WordPress sur le cluster Kubernetes. Une fois déployé, l'URL du service WordPress est récupérée et affichée.

**Configuration de WordPress**

Configurer WordPress après le déploiement. Cette étape vérifie la présence des pods WordPress en cours d'exécution, installe WP CLI, et configure WordPress en ajoutant un utilisateur administrateur si nécessaire. Le plugin WP Statistics est également installé et activé.

### Monitoring

**Déploiement de Prometheus**

Déployer Prometheus en utilisant Helm. Cette étape ajoute le dépôt de Helm pour Prometheus, met à jour les dépôts, et installe Prometheus avec les configurations nécessaires. L'URL du service Prometheus est récupérée et affichée.

**Déploiement de Grafana**

Déployer Grafana en utilisant Helm. Cette étape ajoute le dépôt de Helm pour Grafana, met à jour les dépôts, et installe Grafana avec les configurations nécessaires. L'URL du service Grafana est récupérée et affichée, puis sauvegardée dans un fichier d'environnement.

**Nettoyage de Grafana**

Nettoyer les anciennes clés API de Grafana. Cette étape récupère les clés API existantes dans Grafana et supprime les clés obsolètes nommées "gitlab-ci-key".

**Configuration de Grafana**

Configurer Grafana en créant une clé API. Cette étape crée une nouvelle clé API avec les permissions d'administrateur pour les futures configurations et sauvegarde cette clé dans un fichier d'environnement.

**Configuration des sources de données de Grafana**

Configurer les sources de données dans Grafana. Cette étape vérifie et crée les sources de données pour Prometheus et MySQL dans Grafana. Si les sources de données existent déjà, elles sont mises à jour si nécessaire.

**Importer et personnaliser les tableaux de bord Grafana**

Importer les tableaux de bord et les personnaliser. Cette étape télécharge les templates de tableaux de bord depuis Grafana.com, les importe dans Grafana, et effectue les modifications nécessaires, comme la mise à jour des sources de données et la modification des requêtes SQL.

Nettoyage après toutes les tâches de monitoring
-----------------------------------------------

Nettoyer l'environnement après les tâches de monitoring. Cette étape finale supprime la clé API de Grafana utilisée pendant les configurations pour assurer la sécurité.

En résumé, ce pipeline CI/CD automatisé gère la construction, le déploiement, la configuration, la surveillance, et la sauvegarde de l'application WordPress dans un environnement Kubernetes, avec une intégration complète des outils de monitoring comme Prometheus et Grafana.

### Backup

**Sauvegardes (à finaliser)**

Exécuter les sauvegardes de la base de données, du volume persistant et des configurations Kubernetes. Sauvegardes automatisées avec gitlab schedule vers un bucket S3.